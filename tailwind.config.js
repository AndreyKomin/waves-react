/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx}", // Note the addition of the `app` directory.
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
    "./views/**/*.{js,ts,jsx,tsx}",

    // Or if using `src` directory:
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        bloody: "#571816",
        "bloody-light": "#e54540",
        "dark-red": "#591D31",
        "red-purple": "#512948",
        "custom-purple": "#423656",
        "custom-dark-blue": "#33405B",
        "sage-green": "#2F4858",
        "dark-3": "#2d2936",
        "dark-4": "#1a1621",
        "dark-5": "#0B0B0B",
      },
    },
  },
}
