import {useEffect, useState} from "react";


const useTransaction=({transactionData, signer, onSuccess, onError, onStart}) => {
    const [ready, setReady] = useState();
    const [txLoad, setLoad] = useState(false)
    const [txData, setTxData] = useState(false)
    const [txWait, setWait] = useState(false)
    const [data, setData] = useState(false)

    function triggerTx() {
        onStart()
    }

    useEffect(() => {

    }, [signer])

    return {
        ready,
        onStart,
        triggerTx,
        txLoad,
        txData,
        txWait,
        data,
        error,
    }
}
