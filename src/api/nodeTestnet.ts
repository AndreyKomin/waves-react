import axiosInstance from '../axios/axiosInstance'

import camelcaseKeys from "camelcase-keys";

console.log(axiosInstance)
export const getAccountNFTs = async ({ user }) =>
    axiosInstance
        .get(`/assets/nft/${user?.address}/limit/${100}`)
        .then((res) => camelcaseKeys(res, { deep: true }));

export const getNodeData = async (key) =>
    axiosInstance
        .get(`/addresses/data/${process.env.REACT_APP_WAVES_CONTRACT_ADDRESS}/${key}`)
        .then((res) => camelcaseKeys(res.data, { deep: true }));
