import React from 'react'
import KeeperLogo from '@assets/img/collections_IJUS6efsaCZ8dr27z9I9_icon_sqO9yOQfpJpkivlFG152_512x512_logo_white.webp'

const KeeperIcon = ({ classname }) => {
  return <img className={classname} src={KeeperLogo} />
}

export default KeeperIcon
