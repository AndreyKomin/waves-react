import React from 'react'
import MetamaskLogo from '@assets/img/MetaMask_Fox.svg.png'

const MetamaskIcon = ({ classname }) => {
    return <img className={classname} src={MetamaskLogo} />
}

export default MetamaskIcon
