import React from 'react'

import { useContext, useEffect, useState } from 'react'
import NftList from '../../components/NftList'
import { ButtonPrimary } from '@ui/buttons/ButtonPrimary'
import { MultiContext } from '@context/multiContext'
// import { BigNumber } from 'ethers'
import TransactionSpinner from '@components/TransactionSpinner'
import { CLAIM_LEND_TRANSACTIONS } from '@constants/transactionsSteps'

function MyLendPage() {
  const {
    data: { user, nodeData, signer, myLends },
      actions: {triggerTransaction}
  } = useContext(MultiContext) as any

  const [transactionStep, setTransactionStep] = useState(undefined)
  const [data, setData] = useState([])
  const [isLoading, setLoading] = useState(true)
  const [hasLoaded, setHasLoaded] = useState(false)

  const claimRewards = async (lendId) => {
      const txData = {
          payment: [],
          call: {
              function: 'closeLend',
              args: [
                  { type: 'integer', value: Number(lendId) },
              ],
          },
      }
      await triggerTransaction({ txData, signer, setTransactionStep })
  }

  useEffect(() => {
    if (myLends && user?.address) {
      setData(myLends)
      setLoading(false)
      setHasLoaded(true)
    }
  }, [user, nodeData])


  const nftContent = (nft) => (
    <div>
      <ButtonPrimary onClick={() => claimRewards(nft.lendId)}>
        Claim Rewards & Close Lend
      </ButtonPrimary>
    </div>
  )

  const getBadges = (nft) => {
    const { endTimestamp } = nft

    const nowTimestamp = Math.round(Date.now() / 1000)

    const badges = []
    if (nowTimestamp >= endTimestamp) {
      badges.push({
        badgeType: 'expiration',
        position: 'top-right',
        content: 'ENDED',
      })
    }

    return badges
  }

  return (
    <>
      {transactionStep && (
        <TransactionSpinner
          transactionStep={transactionStep}
          onClose={() => {
            setTransactionStep(undefined)
          }}
        >
          {CLAIM_LEND_TRANSACTIONS[transactionStep]}
        </TransactionSpinner>
      )}
      <NftList
        items={data}
        isLoading={isLoading}
        hasLoaded={hasLoaded}
        badges={getBadges}
        nftContent={nftContent}
      />
    </>
  )
}
export default MyLendPage
