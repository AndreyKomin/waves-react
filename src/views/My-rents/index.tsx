import React from 'react'
import { useContext, useEffect, useState } from 'react'
import NftList from '@components/NftList'
import { MultiContext } from '@context/multiContext'

function MyRentsPage() {
  const {
    data: { user, myRents },
  } = useContext(MultiContext) as any

  const [data, setData] = useState([])
  const [isLoading, setLoading] = useState(true)
  const [hasLoaded, setHasLoaded] = useState(false)

  useEffect(() => {
    if (myRents && user) {
      setData(myRents)
      setLoading(false)
      setHasLoaded(true)
    }
  }, [user, myRents])

  const nftContent = (nft) => (
    <div className='flex flex-col'>
      <span>End Time: {new Date(Number(nft.rent.endTimestamp)).toLocaleTimeString()}</span>
      <span>Start Time: {new Date(Number(nft.rent.startTimestamp)).toLocaleTimeString()}</span>
      <span>Time Unit Count: : {Number(nft.rent.timeUnitCount)}</span>
    </div>
  )

  return (
    <NftList items={data} isLoading={isLoading} hasLoaded={hasLoaded} nftContent={nftContent} />
  )
}
export default MyRentsPage
