import React from 'react'

import { useContext, useEffect, useState } from 'react'
import LendModal from '../../components/Modals/LendModal'
import NftList from '../../components/NftList'
import { MultiContext } from '@context/multiContext'
import { getAccountNFTs } from '../../api/nodeTestnet'
import { ButtonPrimary } from '@ui/buttons/ButtonPrimary'

const getMyNfts = async (user) => {
  const response = await getAccountNFTs({ user })

  return response.data || []
}

function NFtListPage() {
  const {
    data: { user, myNfts },
  } = useContext(MultiContext) as any

  const [isLoading, setIsLoading] = useState(true)
  const [hasLoaded, setHasLoaded] = useState(false)
  const [showModal, setShowModal] = useState(false)
  const [activeToken, setActiveToken] = useState(null)


  useEffect(() => {
    if (myNfts && user?.address) {
      setIsLoading(false)
      console.log(myNfts)
      setHasLoaded(true)
    }
  }, [myNfts, user])

  const onLendStart = (nft) => {
    setShowModal(true)
    setActiveToken(nft)
  }

  const onLendEnd = () => {
    setShowModal(false)
    setActiveToken(null)
  }
  const nftContent = (nft) => (
    <div>
      <ButtonPrimary onClick={() => onLendStart(nft)}>Lend</ButtonPrimary>
    </div>
  )

  return (
    <div>
      {showModal && <LendModal token={activeToken} onClose={onLendEnd} />}

      <NftList items={myNfts} isLoading={isLoading} hasLoaded={hasLoaded} nftContent={nftContent} />
    </div>
  )
}
export default NFtListPage
