import React, { useContext, useEffect } from 'react'

import { useState } from 'react'
import { ButtonPrimary } from '@ui/buttons/ButtonPrimary'
import RentModal from '@components/Modals/RentModal'
import NftList from '@components/NftList'
import { getNodeData } from '../../api/nodeTestnet'
import { MultiContext } from '@context/multiContext'

function RentalsPage() {
  const {
    data: { user, rentals },
    actions: { setWalletsPopup },
  } = useContext(MultiContext) as any
  const hasLoaded = true

  const [activeToken, setActiveToken] = useState(null)
  const [showModal, setShowModal] = useState(false)
  const [availableRents, setAvailableRents] = useState([])
  const [availableRentsLoading, setAvailableRentsLoading] = useState(true)
  const onRentStart = (nft) => {
    if (!user) {
      setWalletsPopup(true)
      return
    }
    setShowModal(true)
    setActiveToken(nft)
  }

  useEffect(() => {
    if (rentals) {
      setAvailableRents(rentals)
      setAvailableRentsLoading(false)
    }
  }, [user, rentals])

  const onLendEnd = () => {
    setShowModal(false)
    setActiveToken(null)
  }

  const nftContent = (nft) => (
    <div>
      <ButtonPrimary onClick={() => onRentStart(nft)}>Rent</ButtonPrimary>
    </div>
  )

  return (
    <div>
      {showModal && <RentModal token={activeToken} onClose={onLendEnd} />}

      <NftList
        items={availableRents}
        isLoading={availableRentsLoading}
        hasLoaded={hasLoaded}
        nftContent={nftContent}
      />
    </div>
  )
}
export default RentalsPage
