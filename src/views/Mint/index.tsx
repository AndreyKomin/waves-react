import React, { useState } from 'react'

import { useContext } from 'react'
import { ButtonPrimary } from '@ui/buttons/ButtonPrimary'
import { MultiContext } from '@context/multiContext'
import TransactionSpinner from '@components/TransactionSpinner'
import { MINT_TRANSACTION } from '@constants/transactionsSteps'

function MintPage() {
  const {
    data: { user, signer },
    actions: { triggerTransaction, setWalletsPopup },
  } = useContext(MultiContext) as any
  const [transactionStep, setTransactionStep] = useState(undefined)
  const MintNfts = async () => {
    if (!user) {
      setWalletsPopup(true)
      return
    }
    const txData = {
      payment: [],
      call: {
        function: 'mintNFT',
        args: [{ type: 'string', value: user?.address }],
      },
    }
    await triggerTransaction({ txData, signer, setTransactionStep })
  }
  const MintToken = async () => {
    if (!user) {
      setWalletsPopup(true)
      return
    }
    const txData = {
      payment: [],
      call: {
        function: 'mintTokens',
        args: [
          { type: 'string', value: user?.address },
          { type: 'integer', value: 1000000000 },
        ],
      },
    }
    await triggerTransaction({ txData, signer, setTransactionStep })
  }

  return (
    <div className='flex justify-center'>
      {transactionStep && (
        <TransactionSpinner
          transactionStep={transactionStep}
          onClose={() => {
            setTransactionStep(undefined)
          }}
        >
          {MINT_TRANSACTION[transactionStep]}
        </TransactionSpinner>
      )}
      <div className='flex flex-col gap-10'>
        <ButtonPrimary
          style={{
            padding: 20,
            fontSize: 24,
            fontWeight: 'bold',
            textTransform: 'uppercase',
          }}
          onClick={MintNfts}
        >
          Mint Free NFT For Testing
        </ButtonPrimary>
        <ButtonPrimary
          style={{
            padding: 20,
            fontSize: 24,
            fontWeight: 'bold',
            textTransform: 'uppercase',
          }}
          onClick={MintToken}
        >
          Mint Free Tokens For Testing
        </ButtonPrimary>
      </div>
    </div>
  )
}
export default MintPage
