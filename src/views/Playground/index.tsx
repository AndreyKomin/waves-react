import React from 'react'
import { Signer } from '@waves/signer'
import { ProviderMetamask } from '@waves/provider-metamask'
import { ProviderKeeper } from '@waves/provider-keeper'
import '../../App.css'
import { useEffect, useState } from 'react'
import { ButtonPrimary } from '@ui/buttons/ButtonPrimary'
import {getAccountNFTs, getNodeData} from '../../api/nodeTestnet'
import { TextInput } from '@ui/inputs/TextInput'
import { LabelText } from '@ui/typography/LabelText'
import { DurationInput } from '@ui/inputs/DurationInput'

function WavesPlayground() {
  const [keeper, setKeeper] = useState()
  const [signer, setSigner] = useState()
  const [user, setUser] = useState()
  const [durationOption, setDurationOption] = useState(60)
  const [lendForm, setLendForm] = useState({
    assetId: '',
    timeUnitPrice: '',
    timeUnitSeconds: 60,
    timeUnitCount: '',
  })


  const connectKeeper = async () => {
    const keeper = new ProviderKeeper()
    const signerProv = await signer.setProvider(keeper)
    const user = await signer.login()
    console.log(user)
    setUser(user)
  }

  const connectMetamask = async () => {
    if (window) {
      const keeper = new ProviderMetamask()
      const signerProv = await signer.setProvider(keeper)
      const user = await signer.login()
      console.log(user)
      setUser(user)
    }
  }

  const getBalance = async () => {
    console.log(signer)
    const balances = await signer.getBalance()
    console.log(balances)
  }

  const MintNfts = async () => {
    const data = {
      dApp: process.env.REACT_APP_WAVES_CONTRACT_ADDRESS,
      payment: [],
      call: {
        function: 'Mint',
        args: [
          { type: 'string', value: user?.address },
          { type: 'string', value: 'ipfs://' },
        ],
      },
    }
    const [tx] = await signer.invoke(data).broadcast()
    console.log(tx)
    //TODO: see waitTxConfirm in waves docs https://docs.waves.tech/ru/building-apps/waves-api-and-sdk/client-libraries/signer#waittxconfirm
    const s = await signer.waitTxConfirm(tx, 3).then((tx) => {
      console.log('wait mint done')
      console.log(tx)
    });
    // console.log(br);
  }
  const lendNft = async () => {
    const data = {
      dApp: process.env.REACT_APP_WAVES_CONTRACT_ADDRESS,
      payment: [],
      call: {
        function: 'initLend',
        args: [
          { type: 'string', value: lendForm.assetId },
          { type: 'integer', value: lendForm.timeUnitSeconds },
          { type: 'integer', value: Number(lendForm.timeUnitPrice) },
          { type: 'integer', value: lendForm.timeUnitCount },
        ],
      },
    }
    console.log(lendForm);
    const [tx] = await signer.invoke(data).broadcast()
    console.log(tx)
    //TODO: see waitTxConfirm in waves docs https://docs.waves.tech/ru/building-apps/waves-api-and-sdk/client-libraries/signer#waittxconfirm
    const s = await signer.waitTxConfirm(tx, 3).then((tx) => {
      console.log('wait mint done')
      console.log(tx)
    });
    // console.log(br);
  }

  const getNfts = async () => {
    const response = await getAccountNFTs({ user })
    console.log('getNfts')
    console.log(response)
  }

  const parseValue = (value) => {
    if (typeof value === "number") {
      return Number(value)
    }

    if (typeof value === "string") {
      try {
        return JSON.parse(value)
      } catch {
        return value
      }
    }

    return value
  }
  const getNodeValues = async () => {
    getNodeData("").then(data => {
      let res = {}

      const regex = /([\w]+)(?=\-[\w\d]+)?/gm

      res = data.reduce((acc, { key, value }) => {
        const isLevelled = key.includes("-")

        if (!isLevelled) {
          acc[key] = parseValue(value)

          return acc
        }

        const levels = key.match(regex)

        let link = acc

        levels.forEach((l, index) => {
          const isLast = levels.length - 1 === index
          if (!isLast) {
            link = link[l] = link[l] ? link[l] : {}
          } else {
            link[l] = parseValue(value)
          }
        })

        return acc
      }, {})
      console.log(res)
      return res
    })
  }
  useEffect(() => {
    // INIT AND CONFIGURE WAVES SIGNER
    if (window) {
      const signer = new Signer({
        NODE_URL: process.env.REACT_APP_WAVES_NODE_API,
        chainId: 84,
      })
      setSigner(signer)
      console.log(signer)
      console.log(keeper)
    }
  }, [])

  return (
    <div className='playground-page'>
      <h1>Waves Playground</h1>
      <div className='flex flex-col gap-10'>
        <ButtonPrimary className='w-[200px]' onClick={() => connectKeeper()}>
          Connect keeper
        </ButtonPrimary>
        <ButtonPrimary className='w-[200px]' onClick={() => connectMetamask()}>
          Connect Metamask
        </ButtonPrimary>
        <ButtonPrimary className='w-[200px]' onClick={() => getBalance()}>
          Get Balance
        </ButtonPrimary>
        <ButtonPrimary className='w-[200px]' onClick={() => MintNfts()}>
          Mint
        </ButtonPrimary>
        <ButtonPrimary className='w-[200px]' onClick={() => getNfts()}>
          get users nft
        </ButtonPrimary>

        <div className='w-[300px]'>
          <h1>Lend</h1>
          <span>Asset id</span>
          <TextInput placeholder='Asset id' value={lendForm.assetId} onChange={e => {
            setLendForm({
              ...lendForm,
              assetId: e.target.value
            })
          }}/>
          <div className='form-field mb-3'>
            <div className='flex w-full items-center mb-2'>
              <LabelText>Time Unit Seconds</LabelText>
            </div>
            <DurationInput
              selectorPosition='from-bottom'
              defaultAmount={durationOption}
              defaultOption='seconds'
              handleChange={({ timeUnitSeconds, timeUnitCount }) =>
                setLendForm({
                  ...lendForm,
                  timeUnitSeconds,
                  timeUnitCount,
                })
              }
            />
          </div>
          <div className='form-field mb-3'>
            <div className='flex w-full items-center mb-2'>
              <LabelText>Price per Time Unit</LabelText>
            </div>
            <TextInput
              type='number'
              value={lendForm.timeUnitPrice}
              onChange={(e) => {
                setLendForm({ ...lendForm, timeUnitPrice: e.target.value })
              }}
              placeholder='Reward per time unit'
            />
          </div>
        </div>
        <ButtonPrimary className='w-[200px]' onClick={() => lendNft()}>
          Lend
        </ButtonPrimary>
      </div>
      <ButtonPrimary onClick={() => getNodeValues()}>Get user Lends</ButtonPrimary>
    </div>
  )
}

export default WavesPlayground
