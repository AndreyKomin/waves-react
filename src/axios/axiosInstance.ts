import axios from "axios";

console.log(process.env.REACT_APP_WAVES_NODE_API);
const axiosInstance = axios.create({
    baseURL: process.env.REACT_APP_WAVES_NODE_API,
});

export default axiosInstance;
