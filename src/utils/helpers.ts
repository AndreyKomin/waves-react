import {getNodeData} from "../api/nodeTestnet";


export const getMediaLink = (link: string) => {
  if (!link) return "";
  if (link.startsWith("ipfs")) {
    return `https://cloudflare-ipfs.com/ipfs${link.slice(link.indexOf("/") + 1)}`;
  }
  return link;
};

export const parseValue = (value) => {
  if (typeof value === 'number') {
    return Number(value)
  }

  if (typeof value === 'string') {
    try {
      return JSON.parse(value)
    } catch {
      return value
    }
  }

  return value
}



export const getNodeValues = (data) => {
  let res = {}

  const regex = /([\w]+)(?=\-[\w\d]+)?/gm

  res = data.reduce((acc, {key, value}) => {
    const isLevelled = key.includes('-')

    if (!isLevelled) {
      acc[key] = parseValue(value)

      return acc
    }

    const levels = key.match(regex)

    let link = acc

    levels.forEach((l, index) => {
      const isLast = levels.length - 1 === index
      if (!isLast) {
        link = link[l] = link[l] ? link[l] : {}
      } else {
        link[l] = parseValue(value)
      }
    })

    return acc
  }, {})
  return res
}
export const extrudeMyLends = ({nodeData, user}) => {
  const values = Object.values(nodeData.lend).filter((item) => item.assetdId)
  const keys = Object.keys(nodeData.lend).filter((item) => Number(item))
  const lends = values
      .map((item, i) => {
        return { ...item, lendId: keys[i] }
      })
      .filter((item) => {
        return item.owner === user?.address
      })
  return lends
}
export const extrudeRentals = ({nodeData}) => {
  const values = Object.values(nodeData.lend).filter((item) => item.assetdId)
  const keys = Object.keys(nodeData.lend).filter(item => Number(item))
  const rentals = values.map((item, i) => {
    return {...item, lendId: keys[i]}
  })
  return rentals
}

export const extrudeMyRents = ({nodeData, user}) => {
    const extractRents = Object.keys(nodeData.lend?.rent).map((lendId) => {
      return Object.values(nodeData.lend?.rent[lendId]).map((item) => ({...item, lendId}))
    }).flat()
    const valuesRent = extractRents.filter((item) => {
      const notEnded =
          Number(item.endTimestamp) > new Date().getTime()
      const accountsNfts = item.customer === user?.address
      return notEnded && accountsNfts
    })
    const myRents = valuesRent.map((item, i) => {
      return {...nodeData.lend[item.lendId], lendId: item.lendId, rent: item}
    })
  console.log(extractRents)
  console.log(valuesRent)
  console.log(myRents)
    return myRents
}
