import axios from 'axios'
import { ethers, utils } from 'ethers'
import { getMediaLink } from './helpers'

/**
 * Ensures the object is an array.
 *
 * @param {*} obj
 * @returns {Array}
 */
export const ensureArray = (obj) => {
  if (!obj) {
    return []
  }
  return Array.isArray(obj) ? obj : [obj]
}

const truncateRegex = /^([a-zA-Z0-9]{4})[a-zA-Z0-9]+([a-zA-Z0-9]{4})$/
/**
 * Truncates an ethereum address to the format 0x0000…0000
 * @param address Full address to truncate
 * @returns Truncated address
 */
export const truncateAddress = (address: string): string => {
  const match = address.match(truncateRegex)
  if (!match) return address
  const [, firstPart, lastPart] = match
  return `${firstPart}\u2026${lastPart}`
}

export const getTokensMeta = async (items: any[], onDone: Function) => {
  const metadata = await Promise.all(
    items?.map(async (token) => {
      if (
        token?.tokenURI ||
        token?.tokenURI !== 'ipfs://' ||
        !JSON.parse(token?.description).tokenURI === 'ipfs://'
      ) {
        const tokenMeta = await axios.get(
          getMediaLink(token.tokenURI || JSON.parse(token?.description).tokenURI),
        ).catch(() => {
          return undefined;
        })
        return tokenMeta
      }
      return undefined
    }),
  )
  onDone(
    items.map((token, i) => ({
      ...token,
      metadata: metadata[i]?.data,
    })),
  )
}

export const toPrice = (value: string) => {
  if (!value) return value
  // debugger
  const formatted = ethers.utils.parseUnits(value, 18)
  return formatted
}

export const fromPrice = (bigNumber) => {
  if (!bigNumber) return ''

  return utils.formatUnits(bigNumber.toString(), 18)
}

export const toTimestamp = (value: number) => Math.round(value / 1000)
