import { Link } from 'react-router-dom'

export const LAST_STEP = 'finish'
export const CANCEL_STEP = 'cancel'
export const ERROR_STEP = 'error'
export const START = 'start'
export const WAIT = 'wait'

const start = (
  <>
    <h2 className='text-4xl mb-7'>Approving</h2>
    <p className='text-lg mb-7'>Accept to proceed.</p>
  </>
)

const approveInProgress = <h2 className='text-4xl mb-7'>Approving in progress...</h2>

const commonMessages = {
  [CANCEL_STEP]: <h2 className='text-4xl mb-7'>Transaction canceled</h2>,
  [ERROR_STEP]: (
    <>
      <h2 className='text-4xl mb-7'>Error!</h2>
      <p className='text-lg mb-7'>Something went wrong, please try again</p>
    </>
  ),
}

export const INIT_LEND_TRANSACTIONS = {
  start,
  [WAIT]: <h2 className='text-4xl mb-7'>Put on lease in progress...</h2>,
  [LAST_STEP]: (
    <>
      <h2 className='text-4xl mb-7'>Success!</h2>
      <p className='text-lg mb-7'>You've put on lease your NFT.</p>
      <Link to='/rentals/my-lends' className='link'>
        Check out on my lends page
      </Link>
    </>
  ),
  ...commonMessages,
}

export const INIT_RENT_TRANSACTIONS = {
  start,
  [WAIT]: <h2 className='text-4xl mb-7'>Rent initializing is in progress...</h2>,
  [LAST_STEP]: (
    <>
      <h2 className='text-4xl mb-7'>Success!</h2>
      <p className='text-lg mb-7'>You've rented the NFT.</p>
      <Link className='link' to='/rentals/my-rents'>
        Check out on my rents page
      </Link>
    </>
  ),
  ...commonMessages,
}

export const CLAIM_LEND_TRANSACTIONS = {
  start,
  [WAIT]: approveInProgress,
  [LAST_STEP]: (
    <>
      <h2 className='text-4xl mb-7 text-center'>Success!</h2>
      <p className='text-lg mb-7'>Rewards have been credited to your wallet</p>
    </>
  ),
  ...commonMessages,
}
export const MINT_TRANSACTION = {
  start,
  [WAIT]: (
    <>
      <h2 className='text-4xl mb-7 text-center'>Mint transaction in progress</h2>
    </>
  ),
  [LAST_STEP]: (
    <>
      <h2 className='text-4xl mb-7 text-center'>Success!</h2>
      <p className='text-lg mb-7'>Rewards have been credited to your wallet</p>
    </>
  ),
  ...commonMessages,
}
