import React from 'react'
import { ProviderMetamask } from '@waves/provider-metamask'
import { ProviderKeeper } from '@waves/provider-keeper'
import KeeperIcon from '@assets/icons/Keeper'
import MetamaskIcon from "@assets/icons/Metamask";

export const METAMASK = 'Metamask'
export const KEEPER = 'Keeper'

export const WALLETS = [
  { title: METAMASK, provider: ProviderMetamask, icon: <MetamaskIcon classname="h-[52px] mb-6"/> },
  { title: KEEPER, provider: ProviderKeeper, icon: <KeeperIcon classname="h-[52px] mb-6"/> },
]
