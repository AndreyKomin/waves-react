export type DurationOption = "seconds" | "hours" | "days" | "weeks" | "months";

export const DURATION_DATA: { [key in DurationOption]: number } = {
  seconds: 1,
  hours: 3600,
  days: 86400,
  weeks: 604800,
  months: 2419200,
};
