import React from 'react'
import { ButtonPrimary } from "@ui/buttons/ButtonPrimary";
import {
  CANCEL_STEP,
  ERROR_STEP,
  LAST_STEP,
} from "@constants/transactionsSteps";
import s from "./TransactionSpinner.module.css";

function TransactionSpinner({ children, onClose, transactionStep }) {
  const closeLoader =
    transactionStep === CANCEL_STEP ||
    transactionStep === LAST_STEP ||
    transactionStep === ERROR_STEP;
  return (
    <div className="w-full h-full absolute left-0 top-0 z-10 flex flex-col items-center justify-center bg-dark-4 bg-opacity-90">
      {children}
      {/* spinner */}
      {!closeLoader && (
        <div className={s.dots}>
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
        </div>
      )}
      {closeLoader && <ButtonPrimary onClick={onClose}>Close</ButtonPrimary>}
    </div>
  );
}

export default TransactionSpinner;
