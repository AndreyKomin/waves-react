import React from "react";

function EmptyState({ children }: { children: React.ReactNode }) {
  return (
    <div className="w-full p-24 text-center">
      <h2 className="text-2xl">{children}</h2>
    </div>
  );
}

export default EmptyState;
