import React from 'react'
import HeroText from "./HeroText";

export default function Hero() {
  return (
    <div className="w-full p-24">
      <HeroText />
    </div>
  );
}
