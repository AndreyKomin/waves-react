import React from 'react'
import { Link } from "react-router-dom";

export default function HeroText() {
  return (
    <div>
      <h1 className="text-7xl font-extrabold text-transparent bg-clip-text bg-gradient-to-br from-cyan-700 to-blue-400">
        Seamless & <br />
        Powerful NFT <br />
        Rentals on Waves
      </h1>
      <div className="mt-10" />
      <Link
        className="text-2xl font-extrabold underline from-cyan-700 to-blue-400"
        to="/rentals"
      >
        Go Lend & Rent
      </Link>
    </div>
  );
}
