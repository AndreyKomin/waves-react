import React from 'react'
import Card from '@components/Card'
import LoadingSkeletonItems from '@ui/LoadingSkeletonItems'
import CardsGrid from '@components/CollectionsGrid'
import { useEffect, useState } from 'react'
import {ensureArray, getTokensMeta, truncateAddress} from "../../utils";
import {getMediaLink} from "../../utils/helpers";

function NftList(props) {
  const { isLoading, hasLoaded, nftContent, badges } = props
  const items = ensureArray(props.items)
  const [nftList, setNftLists] = useState(undefined)
  const noItems = hasLoaded && items.length === 0 && !isLoading
  useEffect(() => {
    if (!isLoading) {
      getTokensMeta(items, setNftLists).then()
    }
  }, [isLoading])
  return (
    <>
      {noItems && <span>No items found</span>}
      <CardsGrid>
        {!isLoading && !noItems && nftList
          ? nftList.map((nft) => {
              let header = nft?.name || nft.metadata?.name

              if (nft.assetId) {
                header += ` ${truncateAddress(nft?.assetId)}`
              }

              return (
                <Card
                  key={nft.assetId}
                  className='cursor-pointer transition-colors'
                  hero={
                    <div className='flex h-full w-full items-center justify-center'>
                      <img
                        className='max-h-full rounded-xl'
                        src={getMediaLink(nft.metadata?.image)}
                      />
                    </div>
                  }
                  badges={badges ? badges(nft) : null}
                  header={header}
                  content={nftContent ? nftContent(nft) : null}
                />
              )
            })
          : !noItems && <LoadingSkeletonItems />}
      </CardsGrid>
    </>
  )
}

export default NftList
