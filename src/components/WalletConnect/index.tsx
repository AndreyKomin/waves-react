import React from 'react'
import { WALLETS } from '@constants/wallets'

const WalletConnect = ({ onWalletChange }) => {
  return (
    <div className='fixed flex flex-row items-center justify-center  z-10 top-0 m-auto  w-full h-full left-0'>
      <div className='flex flex-row items-center justify-center gap-5 bg-gray-700  p-10'>
        {WALLETS.map((item) => {
          return (
            <div
              onClick={() => onWalletChange(item)}
              className='cursor-pointer rounded-xl p-6 bg-gray-900 flex flex-col items-center justify-center min-w-[130px]'
            >
              {item.icon}
              <div>{item.title}</div>
            </div>
          )
        })}
      </div>
    </div>
  )
}
export default WalletConnect
