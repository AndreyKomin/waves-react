import React from "react";

function CardsGrid({ children, className }: { children: React.ReactNode }) {
  return (
    <div className="grid w-full grid-cols-1 flex-wrap gap-4 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
      {children}
    </div>
  );
}

export default CardsGrid;
