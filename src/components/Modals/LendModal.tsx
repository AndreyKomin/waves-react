import React from 'react'
import { LabelText } from '@ui/typography/LabelText'
import { TextInput } from '@ui/inputs/TextInput'
import { useContext, useEffect, useState } from 'react'
import { ButtonPrimary } from '@ui/buttons/ButtonPrimary'
import Card from '@components/Card'
import TransactionSpinner from '@components/TransactionSpinner'
import { INIT_LEND_TRANSACTIONS } from '@constants/transactionsSteps'
import { DURATION_DATA, DurationInput } from '@ui/inputs/DurationInput'
import { MultiContext } from '@context/multiContext'
import { getMediaLink } from '../../utils/helpers'
import { toPrice, truncateAddress } from '../../utils'

function LendModal({ token, onClose }) {
  const {
    data: { signer },
    actions: { triggerTransaction },
  } = useContext(MultiContext) as any

  const [transactionStep, setTransactionStep] = useState(undefined)
  const [durationOption, setDurationOption] = useState(60)
  const [lendForm, setLendForm] = useState({
    timeUnitSeconds: 1,
    timeUnitPrice: '',
    timeUnitCount: 60,
  })
  console.log(lendForm)
  const { assetId, name } = token
  const isErc721 = token.ercType === 'ERC721'
  async function lendToken() {
    const txData = {
      payment: [],
      call: {
        function: 'initLend',
        args: [
          { type: 'string', value: assetId },
          {
            type: 'integer',
            value: Number(lendForm.timeUnitSeconds) * Number(lendForm.timeUnitCount) * 1000,
          },
          { type: 'integer', value: Number(lendForm.timeUnitPrice) * 1000000 },
          { type: 'integer', value: lendForm.timeUnitCount },
        ],
      },
    }
    triggerTransaction({ txData, signer, setTransactionStep })
  }

  return (
    <div className='fixed top-0 left-0 w-full h-full flex flex-row z-50 items-center justify-center bg-gray-500 bg-opacity-70'>
      <div className='relative box-border w-3/5 h-4/5 flex flex-col bg-dark-5 p-11 rounded-sm justify-between'>
        {transactionStep && (
          <TransactionSpinner
            transactionStep={transactionStep}
            onClose={() => {
              setTransactionStep(undefined)
              onClose()
            }}
          >
            {INIT_LEND_TRANSACTIONS[transactionStep]}
          </TransactionSpinner>
        )}

        <div className='w-full grid grid-cols-2 gap-12'>
          <div className='flex flex-col justify-between'>
            <div className='w-full flex flex-col'>
              <div className='form-field mb-3'>
                <div className='flex w-full items-center mb-2'>
                  <LabelText>Time Unit Seconds</LabelText>
                </div>
                <DurationInput
                  selectorPosition='from-bottom'
                  defaultAmount={durationOption}
                  defaultOption='seconds'
                  handleChange={({ timeUnitSeconds, timeUnitCount }) => {
                    console.log({ timeUnitSeconds, timeUnitCount })
                    setLendForm({
                      ...lendForm,
                      timeUnitSeconds,
                      timeUnitCount,
                    })
                  }}
                />
              </div>
              <div className='form-field mb-3'>
                <div className='flex w-full items-center mb-2'>
                  <LabelText>Price per Time Unit</LabelText>
                </div>
                <TextInput
                  type='number'
                  value={lendForm.timeUnitPrice}
                  onChange={(e) => {
                    setLendForm({ ...lendForm, timeUnitPrice: e.target.value })
                  }}
                  placeholder='Reward per time unit'
                />
              </div>
            </div>
          </div>
          <Card
            className='cursor-pointer transition-colors w-full h-fit'
            hero={
              <div className='flex h-full w-full items-center justify-center'>
                <img className='max-h-full rounded-xl' src={getMediaLink(token.metadata?.image)} />
              </div>
            }
            header={`${name} ${truncateAddress(assetId)}`}
          />
        </div>
        <div className='flex flex-row w-full justify-between'>
          <ButtonPrimary onClick={() => lendToken()}>Lend Nft</ButtonPrimary>
          <ButtonPrimary onClick={() => onClose()}>Cancel</ButtonPrimary>
        </div>
      </div>
    </div>
  )
}
export default LendModal
