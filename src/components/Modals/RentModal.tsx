import React from 'react'

import { LabelText } from '@ui/typography/LabelText'
import { TextInput } from '@ui/inputs/TextInput'
import { useContext, useEffect, useState } from 'react'
import { ButtonPrimary } from '@ui/buttons/ButtonPrimary'
import Card from '@components/Card'
import TransactionSpinner from '@components/TransactionSpinner'
import { INIT_RENT_TRANSACTIONS } from '@constants/transactionsSteps'
import { getMediaLink } from '../../utils/helpers'
import { MultiContext } from '@context/multiContext'
import { toPrice } from '../../utils'

function RentModal({ token, onClose }) {
  const {
    data: { nodeData, signer, user },
    actions: { triggerTransaction, setWalletPopup },
  } = useContext(MultiContext) as any

  const [rentForm, setRentForm] = useState({
    timeUnitCount: 0,
  })
  const [transactionStep, setTransactionStep] = useState(undefined)
  const { assetId, lendId, timeUnitPrice } = token
  console.log(token)
  async function rentToken() {
    if (!user) {
      setWalletPopup(true)
      return
    }
    const txData = {
      payment: [
        {
          assetId: 'H6n4sm55mWGoYL6zaqNaMbrjEoiLMsaqBuy2YHcLKQvV',
          amount: Number(rentForm.timeUnitCount) * Number(timeUnitPrice),
        },
      ],
      call: {
        function: 'initRent',
        args: [
          { type: 'integer', value: Number(lendId) },
          { type: 'integer', value: rentForm.timeUnitCount },
        ],
      },
    }
    triggerTransaction({ txData, setTransactionStep, signer })
  }

  return (
    <div className='fixed top-0 left-0 w-full h-full flex flex-row z-50 bg-gray-500 bg-opacity-70 items-center justify-center'>
      <div className='relative box-border w-3/5 h-4/5 flex flex-col bg-dark-5 p-11 rounded-sm justify-between'>
        {transactionStep && (
          <TransactionSpinner
            transactionStep={transactionStep}
            onClose={() => {
              setTransactionStep(undefined)
              onClose()
            }}
          >
            {INIT_RENT_TRANSACTIONS[transactionStep]}
          </TransactionSpinner>
        )}
        <div className='grid grid-cols-2 gap-12 bg-dark-5 p-11 rounded-sm'>
          <div className='flex flex-col justify-between'>
            <div className='w-full flex flex-col'>
              <div className='form-field mb-3'>
                <div className='flex w-full items-center mb-2'>
                  <LabelText>Time Unit Count</LabelText>
                </div>
                <TextInput
                  value={rentForm.timeUnitCount}
                  onChange={(e) => {
                    setRentForm({
                      ...rentForm,
                      timeUnitCount: e.target.value,
                    })
                  }}
                  placeholder='Time units count'
                />
              </div>
            </div>
            <div>
              <ButtonPrimary onClick={() => rentToken()}>Rent this Nft</ButtonPrimary>
            </div>
          </div>
          <Card
            className='cursor-pointer transition-colors w-full h-fit'
            hero={
              <div className='flex h-full w-full items-center justify-center'>
                <img className='max-h-full rounded-xl' src={getMediaLink(token.metadata?.image)} />
              </div>
            }
            header={`${token.metadata.name} ${token.assetId}`}
          />
        </div>
      </div>
    </div>
  )
}
export default RentModal
