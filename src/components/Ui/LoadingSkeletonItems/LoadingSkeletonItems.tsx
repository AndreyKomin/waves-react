import Card from "@components/Card";

export default function () {
  return (
    <>
      {Array.from(Array(10), (e, i) => (
        <Card
          key={i}
          className="cursor-pointer transition-colors"
          header={null}
          skeleton
          subHeader={null}
        />
      ))}
    </>
  );
}
