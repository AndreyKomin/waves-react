import React from 'react';
import { Link, NavLink, useLocation } from "react-router-dom";
import Index from "@ui/UserDropdown";
import classNames from "classnames";
import s from "./style.module.css";

function HeaderMain() {
  const location = useLocation();

  const mainItems = [
    { to: "/rentals", title: "Rentals" },
    { to: "/my-nfts", title: "My NFTs" },
    { to: "/mint", title: "Mint NFT" },
  ];

  const rentalItems = [
    { to: "/rentals", title: "Available Rentals" },
    { to: "/rentals/my-rents", title: "My Rents" },
    { to: "/rentals/my-lends", title: "My Lends" },
  ];

  const renderRentalItems = location.pathname.includes("rentals");
  const dontRenderSubheader =
    location.pathname.includes("my-nfts") ||
    location.pathname === "/" ||
    location.pathname === "/mint";

  let subheaderItems = [];

  if (renderRentalItems) {
    subheaderItems = rentalItems;
  }

  return (
    <header className="w-full px-4 py-4 bg-transparent absolute z-40">
      <div className="bg-custom-dark-blue flex min-h-[72px] flex-wrap items-center justify-center gap-4 rounded-xl py-4 px-6 md:justify-between">
        <div className="flex items-center gap-5">
          <Link to="/">
            <img
              className={s.logo}
              src="/logo.svg"
              width={140}
              height={40}
              alt="Logo"
            />
          </Link>
          <div />
          {mainItems.map(({ to, title }, i) => (
            <NavLink
              key={i}
              className={({ isActive }) =>
                classNames([
                  { underline: isActive },
                  "underline-offset-2 text-xl font-extrabold",
                ])
              }
              to={to}
            >
              {title}
            </NavLink>
          ))}
        </div>
        <div className="flex-5 flex items-center justify-end gap-6">
          <Index />
        </div>
      </div>

      {!dontRenderSubheader && (
        <div className="mt-2 bg-custom-dark-blue flex flex-wrap items-center justify-center gap-4 rounded-xl py-4 px-8 md:justify-between">
          <div className="flex items-center gap-5">
            {subheaderItems.map(({ to, title }, i) => (
              <NavLink
                end
                key={i}
                className={({ isActive }) =>
                  classNames([
                    { underline: isActive },
                    "underline-offset-2 text-sm font-extrabold",
                  ])
                }
                to={to}
              >
                {title}
              </NavLink>
            ))}
          </div>
        </div>
      )}
    </header>
  );
}
export default HeaderMain;
