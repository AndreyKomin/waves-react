import classNames from "classnames";
import { ButtonWidths, ButtonColors } from "../../../types";

const { ORANGE, PURPLE, GREEN, BLUE, MAROON, TRANSPARENT } = ButtonColors;

const { NARROW, MID } = ButtonWidths;

interface Props extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  disabled?: boolean;
  children: React.ReactNode;
  onClick: () => void;
  color?: ButtonColors;
  className?: string;
  width?: ButtonWidths;
  style?: React.CSSProperties;
}

export function ButtonPrimary({
  disabled,
  children,
  color = ORANGE,
  onClick,
  className,
  width = MID,
  style,
}: Props) {
  return (
    <button
      disabled={disabled}
      className={classNames(className, [
        width === NARROW ? "w-auto" : `min-w-[120px]`,
        disabled ? "opacity-50" : "",
        {
          "text-sm flex items-center justify-center rounded-lg px-2 py-1.5":
            true,
          "bg-opacity-0 text-gray-400": color === TRANSPARENT,
          "bg-orange-500 text-white transition hover:bg-primary-hover":
            color === ORANGE,
          "bg-green-500 text-black": color === GREEN,
          "bg-purple-500 text-white": color === PURPLE,
          "bg-blue-500 text-black": color === BLUE,
          "bg-red-800 text-white": color === MAROON,
        },
      ])}
      style={style}
      onClick={onClick}
    >
      {children}
    </button>
  );
}
