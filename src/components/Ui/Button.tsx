import React from 'react'
import { ButtonTypes } from "../../types/components";

function ButtonComponent(props: ButtonTypes) {
  const { children } = props;
  return <button {...props}>{children}</button>;
}

export default ButtonComponent;
