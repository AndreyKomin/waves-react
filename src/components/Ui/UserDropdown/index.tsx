import { useContext } from 'react'
import { MultiContext } from '@context/multiContext'
import { ButtonPrimary } from '@ui/buttons/ButtonPrimary'
import { truncateAddress } from '../../../utils'

function UserDropdown() {
  const {
    data: { user },
    actions: { logout, setWalletsPopup },
  } = useContext(MultiContext) as any


  return (
    <div className='flex flex-row gap-6 items-center overflow-visible'>
      {user && (
        <>
          <svg className='w-[24px] h-[24px]' viewBox='0 0 32 32' xmlns='http://www.w3.org/2000/svg'>
            <g data-name='Layer 7' id='Layer_7'>
              <path
                fill='white'
                d='M19.75,15.67a6,6,0,1,0-7.51,0A11,11,0,0,0,5,26v1H27V26A11,11,0,0,0,19.75,15.67ZM12,11a4,4,0,1,1,4,4A4,4,0,0,1,12,11ZM7.06,25a9,9,0,0,1,17.89,0Z'
              />
            </g>
          </svg>
          <div className='overflow-ellipsis overflow-hidden whitespace-nowrap'>
            <b>Account:</b> {truncateAddress(user?.address)}
          </div>
        </>
      )}
      {user ? (
        <ButtonPrimary onClick={() => logout()}>Disconnect</ButtonPrimary>
      ) : (
        <ButtonPrimary onClick={() => setWalletsPopup(true)}>Connect Wallet</ButtonPrimary>
      )}
    </div>
  )
}
export default UserDropdown
