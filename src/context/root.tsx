import React, { createContext, useState } from 'react'
import UserProvider from "@context/userContext";
import ChainContextProvider from "@context/chainContext";
import MultiContextProvider from "@context/multiContext";

export const RootContext = createContext(null)

function MainContext({ children }: { children: React.ReactNode }) {
  return (
    <UserProvider>
      <ChainContextProvider>
        <MultiContextProvider>{children}</MultiContextProvider>
      </ChainContextProvider>
    </UserProvider>
  )
}

export default MainContext
