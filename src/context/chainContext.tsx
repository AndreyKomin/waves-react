import { createContext, useContext, useEffect, useState } from 'react'
import { getAccountNFTs, getNodeData } from '../api/nodeTestnet'
import { ERROR_STEP, LAST_STEP, START, WAIT } from '@constants/transactionsSteps'
import { extrudeMyLends, extrudeMyRents, extrudeRentals, getNodeValues } from '../utils/helpers'
import { UserContext } from '@context/userContext'
import {log} from "util";

export const ChainContext = createContext({ data: {}, actions: {} })

function ChainContextProvider({ children }) {
  const {
    data: { user },
    actions: { setWalletsPopup },
  } = useContext(UserContext)

  //scope
  const [nodeData, setNodeData] = useState(undefined)
  const [nodeDataRaw, setNodeDataRaw] = useState(undefined)
  const [rentals, setRentals] = useState(undefined)

  //user
  const [myRents, setMyRents] = useState(undefined)
  const [myLends, setMyLends] = useState(undefined)
  const [myNfts, setMyNfts] = useState(undefined)

  //
  const triggerTransaction = async ({ txData, setTransactionStep, signer }) => {
    const data = {
      dApp: process.env.REACT_APP_WAVES_CONTRACT_ADDRESS,
      ...txData,
    }
    setTransactionStep(START)
    //client accepts
    const tx = await signer
      .invoke(data)
      .broadcast()
      .then((data) => {
        setTransactionStep(WAIT)
        console.log(data)
        return data
      })
      .catch((err) => {
        setTransactionStep(ERROR_STEP)
        alert(err.message)
        console.log(err.message)
      })
    console.log(tx)
    const txWait = await signer
      .waitTxConfirm(tx, 1)
      .then(async (tx) => {
        console.log(tx)
        await fetchNodeValues()
        await fetchNfts()
        setTransactionStep(LAST_STEP)
      })
      .catch(() => {
        setTransactionStep(ERROR_STEP)
      })
    return txWait
  }
  const fetchNfts = async () => {
    await getAccountNFTs({ user }).then((res) => {
      console.log(res.data)
      const rentalsAssets = rentals.map((item) => item.assetdId)
      setMyNfts(res.data.filter((item) => !rentalsAssets.includes(item.assetId)) || res.data)
    })
  }

  const fetchNodeValues = async () => {
    await getNodeData('').then((data) => {
      setNodeDataRaw(data)
      return data
    })
  }

  useEffect(() => {
    //initial load
    fetchNodeValues().then(async () => {
      if (user?.address) {
        await fetchNfts()
      }
    })

  }, [user?.address])

  useEffect(() => {
    if (nodeDataRaw) {
      console.log('refetched')
      setNodeData(getNodeValues(nodeDataRaw))
    }
  }, [nodeDataRaw])

  useEffect(() => {
    if (nodeData && user?.address) {
      setMyLends(extrudeMyLends({ nodeData, user }))
      setMyRents(extrudeMyRents({ nodeData, user }))
    }

    if (nodeData) {
      setRentals(extrudeRentals({ nodeData }))
    }
  }, [nodeData, user])

  return (
    <ChainContext.Provider
      value={{
        data: { nodeData, myLends, myNfts, myRents, rentals },
        actions: { triggerTransaction },
      }}
    >
      {children}
    </ChainContext.Provider>
  )
}

export default ChainContextProvider
