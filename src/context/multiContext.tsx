import { createContext, useContext } from "react";
import {ChainContext} from "@context/chainContext";
import {UserContext} from "@context/userContext";


export const MultiContext = createContext(null);
// multi context designed to merge several contexts into 1
// if you need some data from context use only MultiContext in components

function MultiContextProvider({ children }) {
    const { data: userContextData, actions: userContextActions } =
        useContext(UserContext);
    const { data: chainContextData, actions: chainContextActions } =
        useContext(ChainContext);

    return (
        <MultiContext.Provider
            value={{
                data: { ...userContextData, ...chainContextData },
                actions: { ...userContextActions, ...chainContextActions },
            }}
        >
            {children}
        </MultiContext.Provider>
    );
}
export default MultiContextProvider;
