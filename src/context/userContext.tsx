import React, { createContext, useEffect, useState } from 'react'
import { Signer } from '@waves/signer'
import { WALLETS } from '@constants/wallets'
import WalletConnect from "@components/WalletConnect";

// wallet connect project id
export const UserContext = createContext({ data: {}, actions: {} })

function UserProvider({ children }) {
  const [signer, setSigner] = useState(undefined)
  const [provider, setProvider] = useState(undefined)
  const [user, setUser] = useState(undefined)
  const [walletsPopup, setWalletsPopup] = useState(false)

  useEffect(() => {
    // INIT AND CONFIGURE WAVES SIGNER
    if (window) {
      const signer = new Signer({
        NODE_URL: process.env.REACT_APP_WAVES_NODE_API,
      })
      setSigner(signer)
    }
  }, [])

  useEffect(() => {
    if (signer) {
      const lastSeenProvider = localStorage.getItem('provider')
      if (lastSeenProvider) {
        const index = WALLETS.findIndex((item) => item.title === lastSeenProvider)
        console.log(index)
        chooseProvider(WALLETS[index]).then()
      }
    }
  }, [signer])
  const chooseProvider = async (wallet) => {
    await signer.setProvider(new wallet.provider())
    localStorage.setItem('provider', wallet.title)
    const user = await signer.login()
    console.log(user)
    setUser(user)
    setWalletsPopup(false);
    return user
  }

  const logout = async () => {
    await signer.logout()
    localStorage.clear()
    setUser(undefined)
  }

  const data = { user, signer, provider }
  const actions = { chooseProvider, setUser, logout, setWalletsPopup }

  return <UserContext.Provider value={{ data, actions }}>
    {walletsPopup && <WalletConnect onWalletChange={chooseProvider}/>}
    {children}
  </UserContext.Provider>
}

export default UserProvider
