import React from 'react'
import './App.css'
import WavesPlayground from './views/Playground'
import HeaderMain from './components/Ui/HeaderMain'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import HomePage from './views/Home'
import MyLendPage from './views/My-lends'
import NFtListPage from './views/My-nfts'
import RentalsPage from './views/Rentals'
import MyRentsPage from './views/My-rents'
import MintPage from './views/Mint'
import MainLayout from './layouts/MainLayout'
import MainContext from "@context/root";

function App() {
  return (
    <div className='App'>
        <MainContext>
            <Router>
                <HeaderMain />
                <MainLayout>
                    <Routes>
                        <Route index path='/' element={<HomePage />} />
                        <Route path='/my-nfts' element={<NFtListPage />} />
                        <Route path='/rentals' element={<RentalsPage />} />
                        <Route path='/rentals/my-lends' element={<MyLendPage />} />
                        <Route path='/rentals/my-rents' element={<MyRentsPage />} />
                        <Route path='/mint' element={<MintPage />} />
                        <Route path='/playground' element={<WavesPlayground />} />
                    </Routes>
                </MainLayout>
            </Router>
        </MainContext>
    </div>
  )
}

export default App
